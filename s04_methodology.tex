\section{Experimental Methodology}
\label{sec:evaluation}
%1. Methodology: talk about the machine, nested virtualization
%2. Benchmarks: CloudSuite

We tested our memory management mechanism in a experimental platform consisting
of three computing nodes. We are using nodes that have different hardware
characteristics, but running the same software stack. Every node as well as the
VMs runs Ubuntu 14.04 with Linux kernel 3.19.0+ as the Operating System, and
Xen 4.5 as the hypervisor. The nodes communicate using TCP/IP sockets over
Ethernet. As explained in Section~\ref{sec:sharing}, one of the nodes in the
system acts as the Memory Manager Master node, which means that it processes
the requests for memory and keeps information on the status of the other nodes.
The hardware properties of the nodes are given in Table~\ref{fig:hardware}.

\begin{table}[t]
    \centering
    \small
    \begin{tabular}{@{}l@{~~}l@{~~}r@{~~}r@{}}
        \toprule
        \textbf{Node} & \textbf{CPU} & \textbf{Frequency} & \textbf{Memory} \\
        \midrule
        Node 1 & AMD FX Quad-Core & \SI{1.4}{\giga\hertz} &  \SI{6}{\giga\byte} \\
        Node 2 & Intel Core i7 & \SI{2.10}{\giga\hertz} & \SI{8}{\giga\byte} \\
        Node 3 & Intel Xeon & \SI{2.262}{\giga\hertz} & \SI{64}{\giga\byte} \\
        \bottomrule
    \end{tabular}
    \caption{Hardware characteristics}
    \label{fig:hardware}
\end{table}

% \begin{enumerate}
%     \item Node 1, CPU: AMD FX Quad-Core Processor, \SI{1.4}{\giga\hertz};  Memory \SI{6}{\giga\byte}
%     \item Node 2, CPU: Intel Core i7, \SI{2.10}{\giga\hertz}; Memory: \SI{8}{\giga\byte};
%     \item Node 3, CPU: Intel Xeon, \SI{2.262}{\giga\hertz}; Memory: \SI{64}{\giga\byte}
% \end{enumerate}

The shared global address space was emulated using the node's local memory.  In
AR-Tmem, the hardware global address space is used only to share memory
capacity, not to share data. We therefore modified Xen to start up using a
portion of the physical memory capacity, equalling the emulated memory capacity
of the node. The rest of the physical memory capacity on the node was reserved
to emulate remote data storage (this was especially possible on
Node~3, which has \SI{64}{\giga\byte} of RAM). Whenever the hypervisor performs
an ``emulated'' remote access, we add a delay loop lasting \SI{50}{\micro\second} to model extra
hardware latency.

We evaluate our memory management mechanism using the CloudSuite 3.0
Benchmarks~\cite{cloudsuite}. Our evaluation process consists of running
multiple DomUs with different benchmarks from the suite, but we may also use
the same benchmark multiple times in different DomUs. In all cases, we execute
at most three DomUs simultaneously, and refer to each distinct set of DomUs as a
\emph{scenario}. Table~\ref{table:scenarios} shows the scenarios that we used
in this paper.

\begin{table*}[t]
\centering
\small
\begin{tabular}{@{}p{1.9cm}>{\raggedright\hspace{0pt}}  p{4.5cm}  p{9.4cm}@{} }
\toprule
\textbf{Scenario} & \textbf{VM Parameters} & \textbf{Description} \\
\midrule
  
Scenario 1 & VM1: \SI{768}{\mega\byte} RAM, 1 CPU & \multirow{3}{*}{\parbox{9.4cm}{Every VM executes simultaneously in-memory-analytics once, sleeps 5 seconds and then executes it again. The data set was taken from \cite{spark-training}. All nodes have \SI{1}{\giga\byte} of Tmem capacity enabled. \newline}} \\
           & VM2: \SI{768}{\mega\byte} RAM, 1 CPU &  \\
           & VM3: \SI{1}{\giga\byte} RAM, 1 CPU&  \\[0.7mm]

Scenario 2      & VM1: \SI{512}{\mega\byte} RAM, 1 CPU & \multirow{3}{*}{\parbox{9.4cm}{VM1 and VM2 start executing \emph{usemem} simultaneously, and VM3 starts when VM1 and VM2 attempt to allocate \SI{640}{\mega\byte} of memory. All are stopped simultaneously when VM3 attempts to allocate \SI{768}{\mega\byte}. Node 2 has \SI{1}{\giga\byte} of Tmem capacity, while Nodes 1 and 3 have \SI{384}{\mega\byte}. }} \\
\emph{(Usemem)} & VM2: \SI{512}{\mega\byte} RAM, 1 CPU & \\
                & VM3: \SI{512}{\mega\byte} RAM, 1 CPU & \\
                & & \\[0.7mm]

Scenario 3 & VM1: \SI{512}{\mega\byte} RAM, 1 CPU & \multirow{3}{*}{\parbox{9.4cm}{Nodes 1 and 3 have VM1 and VM2 running. Every VM executes graph-analytics once. They use the dataset provided by \cite{soc-twitter-follows}, \cite{netdatarep}, \cite{netdatarepinter}. All nodes have \SI{1}{\giga\byte} of Tmem capacity enabled.}} \\
           & VM2: \SI{512}{\mega\byte} RAM, 1 CPU  & \\
           & &\\
  
%\parbox[t]{2cm}{Scenario 4} & \parbox[t]{1cm}{4} & \parbox[t]{4cm}{VM1, VM2: \SI{1}{\giga\byte} of RAM, 1 CPU \\ VM3, VM4: \SI{512}{\mega\byte} of RAM, 1 CPU} & \parbox[t]{8.5cm}{VM1 and VM2 run in-memory-analytics simultaneously, VM3 executes the client of the data-caching benchmark while VM4 executes the memcached server. Communication between VM3 and VM4 is established through IP addresses. VM4 server is launched, while VM1~3 launch their applications simultaneously.} \\
  
  %RB & Dider Domi & Demencia & Demencia\\
  %MC & David Batty & Demencia & Demencia\\
  %MC & Eirik Bakke & Demencia & Demencia\\
  %\hline
\bottomrule  
\end{tabular} 
\caption{List of scenarios used for benchmarking}
\label{table:scenarios}
\end{table*}

We also designed a microbenchmark called \emph{usemem}, which allocates a
varying amount of memory, starting from \SI{128}{\mega\byte} onwards. Every time
it allocates memory, it steps through it performing write/read operations,
while checking the correctness of the values read. Everytime it iterates, it
shows the running time necessary for each iteration, and when an iteration
completes, \emph{usemem} prints out the average running time for all
iterations. We run every scenario for the memory management policy we
implemented. In every scenario, the amount of memory enabled for Tmem at the
master was fixed at \SI{1}{\giga\byte}.

%We also want to keep track of the amount of remote memory that a node is using and the amount of memory that a node is borrowing. For this, we add some instrumentation capabilities to the MM.


% PAUL:
% Running it with virtual box on the hca server. Offer description of both
% Do a brief description of the platform we are running.
% Use an example from Paul's previous papers.
% Describe the benchmarks.
% Metrics to measure: the memory using from a node (local and remote), the number of swap operations used by a single node.

% This paper uses three different memory management policies: the default naive
% policy used in Tmem (greedy-local) with no aggregated remote memory, an
% extended version of the former but with aggregated memory (greedy-remote), and
% a two-tier memory management strategy (TTM). TTM increases the allocation of
% pages to every VM depending on the swapping rate detected by the MM of the
% node. The MM receives memory utilization information from the hypervisor every
% second. The pages allocated to a VM are increased by a percentage $\%P$ of the
% available pages the node has (local or remote), and deallocates them by an
% equal rate once the MM detects that the VMs have stopped swapping.

This paper uses three memory management policies:
\begin{itemize}
\item \textbf{greedy-local}: The baseline is the default naive
    policy used in Tmem with no aggregated remote memory.
\item \textbf{greedy-remote}: An extended version of greedy-local using 
    aggregated memory.

\item \textbf{TTM}: A two-tier memory management strategy that increases the
    memory allocation for each VM depending on its swapping rate visible to the
    node's MM. The pages allocated to a VM are increased by a percentage $\%P$
    of the pages owned by the node (local or remote), and deallocates them
    by an equal rate once the MM detects that the VMs have stopped swapping.

\end{itemize}

  
