\begin{table*}[t]
\centering
\small
\begin{tabular}{@{}p{5cm}  p{12cm}@{}}
\toprule
\textbf{Memory Statistics} & \textbf{Description} \\
\midrule
$ENOMEM$ & Code used in the hypervisor to signify that a $put$ cannot succeed due to a lack of tmem capacity \\[0.7mm]

$node\_info$ & Data structure that holds general status information of the computer host.\\[-0.2mm]
$node\_info.total\_tmem$ & Total number of pages available to tmem (free or allocated)\\[-0.2mm]
$node\_info.free\_tmem$ & Number of free pages available to tmem \\[-0.2mm]
$node\_info.vm\_count$ & Number of VMs registered with tmem \\[0.7mm]

$vm\_data_{\textrm{hyp}}$ & Data structure that holds the parameters of all of the VMs within the hypervisor \\[-0.2mm]
$vm\_data_{\textrm{hyp}}[id].vm\_id$ & Identifier of the VM within Xen \\[-0.2mm]
$vm\_data_{\textrm{hyp}}[id].tmem\_used$ & Number of pages of tmem memory currently used by the VM \\[-0.2mm]
$vm\_data_{\textrm{hyp}}[id].mm\_target$ & Target number of pages for the VM, held by the hypervisor and previously sent by the MM \\[-0.2mm]
$vm\_data_{\textrm{hyp}}[id].puts\_total$ & Total number of $puts$ issued by the VM in the most recent period\\[-0.2mm]
$vm\_data_{\textrm{hyp}}[id].puts\_succ$ & Total number of successful $puts$ issued by the VM in the most recent period\\[0.7mm]

$memstats$ & Data structure that holds the last sampled statistics that the hypervisor sent to the MM.\\[-0.2mm]
$memstats.vm\_count$ & Amount of active VMs as seen by the MM.\\[-0.2mm]
$memstats.prev$ & Pointer to the before last sampled statistics that the hypervisor sent to the VM.\\[-0.2mm]
$memstats.vm$ & Array where each entry holds statistics about an active VM \\[-0.2mm]
$memstats.vm[i].vm\_id$ &Identifier of the VM within the MM, as received from the hypervisor \\[-0.2mm]
$memstats.vm[i].puts\_total$ & Total number of $puts$ that a VM has issued to the hypervisor in the recent period \\[-0.2mm]
$memstats.vm[i].puts\_succ$ & Total number of successful $puts$ that a VM has issued to the hypervisor in the recent period \\[-0.2mm]
$memstats.vm[i].cumul\_puts\_failed$ & Total number of failed $puts$ that a VM has issued since the VM became active \\[0.7mm]

$mm\_out$ & Pointer to a data structure that holds the output parameters of the MM policy \\[-0.2mm]
$mm\_out[i].vm\_id$ & VM identifier that maps a VM to its target allocation as calculated by the MM \\[-0.2mm]
$mm\_out[i].mm\_target$ & Memory allocation target as calculated by the policy in the MM \\

\bottomrule
\end{tabular}
\caption{Summary of memory statistics used in the Memory Manager~(MM)}
\label{table:statvars}
\end{table*}

%\section{AR-Tmem: Aggregation and Management of Memory Capacity}
\section{AR-Tmem design}
\label{sec:sharing}

This section presents the AR-Tmem approach for memory capacity aggregation and
management. Every node inside the computing infrastructure implements a
separate instance of the software architecture of AR-Tmem, which consists of
three components:

\begin{itemize}
    \item Hypervisor support (Section~\ref{sec:sharing:xen})
    \item Tmem Kernel Module in kernel of Dom0 (Section~\ref{sec:dom0tkm})
    \item Memory Manager in user space in Dom0 (Section~\ref{sec:sharing:mm})
\end{itemize}


%% \begin{figure}[!t]
%% \centering
%% \includegraphics[width=3.4in]{sec3-mmswstack}
%% % where an .eps filename suffix will be assumed under latex, 
%% % and a .pdf suffix will be assumed for pdflatex; or what has been declared
%% % via \DeclareGraphicsExtensions.
%% \caption{Software stack for hypervisor-managed memory capacity sharing}
%% \label{fig:sec3-mmswstack}
%% \end{figure}

\subsection{Hypervisor Support}
\label{sec:sharing:xen}

The hypervisor support for AR-Tmem has deliberately been kept to the minimum
and localized in the Tmem subsystem.  First, the hypervisor enforces the
memory consumption constraints determined by the local memory manager.  Second, it must
allocate and deallocate local and remote physical pages and pass ownership of
blocks of pages in and out of the hypervisor.  Thirdly, it must collect
statistics required by the user-space memory managers.

\looseness -1 \textbf{Enforcing local per-VM memory constraints}: The
hypervisor constrains the Tmem memory consumption of its VMs, based on
instructions from the user-space Memory Manager.  In the current design, the
Memory Manager specifies for each VM the maximum number of pages it can use.

\label{sec:allocation}

\textbf{Page allocation and transfer of ownership}: AR-Tmem ensures that each
physical page in the system is \emph{owned} by at most one hypervisor or Memory
Manager.\footnote{Normally a page has exactly one owner, but, as seen in Section~\ref{sec:resiliency} failed nodes may
incur memory leaks.}  The Tmem pages owned by a hypervisor (local and remote)
are allocated using a zoned Buddy allocator, with a separate zone for each node
in the system (including itself) from which it has ownership of at least one
page.\footnote{With realistic assumption and memory capacity sharing among 64
    nodes, the maximum size of this data structure is only about
\SI{10}{\Kilo\byte}.} These allocators should be stored as a tree corresponding
to the hierarchy of the system (nodes in leaves, then, e.g.\ boards,
chasses and racks).

\looseness -1 Figure~\ref{fig:sec3-pagemvmtinlists} shows how physical page ownership is
moved within the hypervisor.  A Tmem \textsf{put} operation uses a depth-first
search among the allocators to allocate a closest free page.  A Tmem
\textsf{get} or \textsf{flush} operation causes a free page to be returned to the
corresponding Buddy allocator.  A \textsf{Grant} hypercall is used when the
Memory Manager receives ownership of a list of blocks (each an
appropriately-aligned power-of-two number of local or remote pages).
These page blocks are added as free blocks to the appropriate
Buddy allocator.  In contrast, a \textsf{Request} hypercall is used to release
ownership of a particular number of pages on behalf of another specified node.
In response, the hypervisor uses a heuristic to allocate as many blocks as
needed (up to the requested number of pages and maximum number of blocks). It
uses a depth-first search among the allocators to take blocks located
physically close to the target node and as large a size as possible.

Two other hypercalls are needed to support (remote) node shutdown and failure.
Both hypercalls may be expensive to implement and, while important, they should
be rarely executed. The \textsf{Return} hypercall releases ownership of a
particular number of pages with a given home node (physical location) that
wishes to shutdown, or otherwise leave AR-Tmem.  It returns free pages, and,
executing asynchronously, searches for pages in use by the Tmem clients and
migrates their contents to free pages.  It periodically returns lists of
blocks, in the same way as for a \textsf{Request}.  The \textsf{Invalidate-Xen}
hypercall is used when a node fails: it removes all free or allocated pages
with a given home node (physical location), and it stops all VMs that were
using such data.  It returns an acknowledgement when done.


\begin{figure}[!t]
\centering
\includegraphics[width=2.5in]{sec3-pagemvmtinlistsv2.pdf}\vspace{-4mm}
\caption{Flow of page ownership inside a node}
\label{fig:sec3-pagemvmtinlists}
\end{figure}

\textbf{Memory statistics}: Table~\ref{table:statvars} presents the statistics
gathered by the hypervisor.  The number of values gathered needs to be kept
to the minimum in order to minimise the overhead of communication from
the hypervisor to the user-space memory management process. 

\subsection{Dom0 Tmem Kernel Module (TKM)}
\label{sec:dom0tkm}

As explained in Section~\ref{sec:tmem}, interfacing to the paravirtualized Tmem
client interface requires a kernel module in each guest domain.
AR-Tmem adds a kernel module in the privileged domain Dom0. The kernel module
in Dom0 acts only as an interface between the hypervisor (using hypercalls) and
the node's Memory Manager (using netlink sockets). 

\subsection{Dom0 User-space Memory Manager~(MM)}
\label{sec:sharing:mm}

Each node has a user-space Memory Manager~(MM) in its privileged domain, Dom0.
The MMs perform most of the work of AR-Tmem by cooperating to:

\begin{enumerate}
    \item Distribute memory owned by each node among its guests
    \item Distribute global memory capacity among nodes
    \item Implement the flow of page ownership among nodes
    % \item Enable nodes to enter and leave the system (for e.g. shutdown and reboot) and handle failures
    \item Enable nodes to join and leave, and handle failures
\end{enumerate}


\subsubsection{Joining the AR-Tmem system}
\label{sec:joining}

In the current design, there is a single centralized Memory Manager
Master~(MM--M) responsible for controlling the system and distributing the
global memory capacity. All Memory Managers communicate using a secure and
reliable packet transport such as SSL/TLS. The types of messages passed among
the Memory Managers are listed in Table~\ref{table:messages}.

In order to be able to join the system, a node
requires a configuration file, which provides the network address of the MM--M,
and it needs security credentials to establish secure connections. It must
obtain (from the configuration file or MM--M) the mapping from slave node ID
to network address. For managing locality, it also needs to know the location of each
node in the NUMA hierarchy.

When a node $R$ wishes to join the AR-Tmem system, it first sends a
\textsf{Register} message to the MM--M (see Table~\ref{table:messages}). The
MM--M creates a representation of the new node, and sets its state to
\textsf{Active}. It then sends a \textsf{Enable-Node}($R$,1) to all of the
registered nodes. Each node maintains a bitmap of the enabled nodes. When a
node receives such a message, it checks whether it already has ownership of any
pages with home (physical location) $R$, which would be a fatal error.

\begin{table*}[t]
    \centering
    \small
    \begin{tabular}{@{}p{2.8cm}@{~~}p{2.0cm}@{~~}p{8.5cm}@{~~}p{3.8cm}@{}}
        \toprule
        \textbf{Command} & \textbf{Direction} & \textbf{Description} & \textbf{Slave state}\\
        \midrule

        \multicolumn{3}{@{}l}{\textit{Distribution of global memory capacity}}\\[-0.2mm]
        \textsf{Statistics(}$S$\textsf{)} & Slave$\rightarrow$Master & Send node statistics $S$ to Master & \textsf{Active}\\[-0.2mm]
        \textsf{Grant-Any(}$n,x$\textsf{)} & Master$\rightarrow$Slave & Request grant of $n$ pages to slave $x$& \textsf{Active}\\[-0.2mm]
        \textsf{Grant-Return(}$n,x$\textsf{)} & Master$\rightarrow$Slave & Request grant of $n$ pages to slave $x$& \textsf{Active}\\[-0.2mm]
        \textsf{Force-Return(}$n,x$\textsf{)} & Master$\rightarrow$Slave & Disable node and return all pages located at slave $x$ (for leaving)& \textsf{Active}\\[-0.2mm]
        \textsf{Mem-Limit(}$n$\textsf{)} & Master$\rightarrow$Slave & Limit the number of pages allocated to store local data & \textsf{Active} \\[0.7mm]


        \multicolumn{3}{@{}l}{\textit{Flow of page ownership}}\\
        \textsf{Grant(}$b, \cdots$\textsf{)} & Slave$\rightarrow$Slave & Transfer ownership of blocks of pages & \textsf{Active}\\[0.7mm]

        \multicolumn{3}{@{}l}{\textit{Node state changes}}\\[-0.2mm]
        \textsf{Register} & Slave$\rightarrow$Master & Register a new node & \textsf{Inactive}$\rightarrow$\textsf{Active}\\[-0.2mm]
        \textsf{Leave-Req} & Slave$\rightarrow$Master & Node requests to leave the system (e.g. for shutdown) & \textsf{Active}$\rightarrow$\textsf{Leaving}\\[-0.2mm]
        \textsf{Leave-Notify} & Master$\rightarrow$Slave & MM--M notifies that the recipient has left the system & \textsf{Leaving}$\rightarrow$\textsf{Inactive}\\[-0.2mm]
        \textsf{Enable-Node(}$x,e$\textsf{)} & Master$\rightarrow$Slave & Inform slave to accept ($e=1$) or reject ($e=0$) pages located at $x$ & \textsf{Active}\\[0.7mm]


        \multicolumn{3}{@{}l}{\textit{Resiliency support}}\\[-0.2mm]
        \textsf{Invalidate} & Slave$\rightarrow$Master & Invalidate all pages located at sending node& \textsf{Active}/\textsf{Leaving}$\rightarrow$\textsf{Recovery}\\[-0.2mm]
        \textsf{Invalidate(}$x$\textsf{)} & Master$\rightarrow$Slave & Request recipient to invalidate pages at node $x$ & \textsf{Active} \\[-0.2mm]
        \textsf{Invalidate-Notify(}$x$\textsf{)} & Master$\rightarrow$Slave & MM--M notifies that the recipient has been invalidated & \textsf{Recovery}$\rightarrow$\textsf{Inactive} \\
        \bottomrule

    \end{tabular}
    \caption{Memory Manager message types}
    \label{table:messages}
\end{table*}

\subsubsection{Distributing ownership of memory}
\label{sec:distributing-ownership}
\textbf{Distributing memory owned by a node among guests}: At each node, the
local Memory Manager~(MM) determines the memory limit (maximum number of 
pages) for each VM.  This is done using a policy based on the statistics from
the hypervisor.  Pages are distributed subject to a total (local plus remote)
memory consumption limit, set by the MM--M using the \textsf{Mem-Limit}
message.  In this paper we evaluate simple policies, and focus attention on an
overall design that enables future work on improving the memory allocation
policies.

\textbf{Distributing global memory capacity among nodes}: All active nodes, i.e.
all nodes in the \textsf{Active} state (Figure~\ref{fig:sec3-states}), regularly send
statistics to the MM--M using the \textsf{Statistics} message.  Based on these
statistics and the global memory policy, the MM--M may redistribute the memory
capacity among nodes by sending the \textsf{Grant-Any} message to the donor
node, which is a request to transfer ownership of a specified number of free
physical pages to a given node. 

\looseness -1 \textbf{Implementing flow of page ownership}: The MM--M
rebalances memory capacity, but it does not need to know the actual physical addresses.
Ownership of global physical addresses is transferred peer-to-peer
using \textsf{Grant} messages. A \textsf{Grant} message passes a list of
blocks, each an appropriately-aligned power-of-two number of physical pages.

To avoid race conditions during node shutdown and following a failure, the
recipient checks the home of each received block against the bitmap of enabled
nodes, defined in Section~\ref{sec:joining}. If the home node of a received
block has been disabled, which happens rarely, then ownership is returned back
to its home using a new point-to-point \textsf{Grant} message.


%% \begin{figure}[t]
%%     \centering
%%     \includegraphics[width=\columnwidth]{borrowing.jpg}
%%     \caption{Example borrowing graph (held at MM--M)}
%%     \label{fig:borrowing-graph}
%% \end{figure}
%% 
%% \subsubsection{Maintaining a ``balanced'' distribution of memory capacity}
%% 
%% The global page ownership is represented using the \emph{borrowing graph},
%% illustrated in Figure~\ref{fig:borrowing-graph}(a).  In this example node~1 has
%% ownership of 5,000 pages that are physically located at node 2, node 2 has
%% ownership of 3,000 pages that are physically located at node 3, and so on.  The
%% borrowing graph is used to (a)~keep track of the memory capacity owned at each
%% node, (b)~avoid suboptimal cases where nodes unnecessarily share physical
%% memory capacity, and (c)~recognise and handle situations where the failure of
%% one node would impact a large number of other nodes.
%% 
%% The borrowing graph is stored at the MM--M and updated using the information in
%% the \textsf{Statistics} messages.  Each node is aware of the physical location
%% of the pages that it owns, so that its \textsf{Statistics} message can
%% correctly indicate its outgoing edges and labels (at the time that the message
%% was sent). A node $x$ does not know its incoming edges, however, since after
%% granting ownership of its pages to another node $y$, $y$ may later grant them
%% to a third node $z$ without informing $x$. Although not shown in
%% Figure~\ref{fig:borrowing-graph}, each node has an edge to itself indicating
%% the number of local pages that it owns.  The information to build the borrowing
%% graph is received periodically at the MM--M and it may be out of date. For this
%% reason, the edge labels are only approximately correct.
%% 
%% \textbf{Preference for bipartite borrowing graph:} An optimal borrowing graph
%% should be bipartite, with the nodes classified into two sets: \emph{borrowers}
%% with at least one outgoing edge (outdegree~$>0$) and \emph{lenders} with at
%% least one incoming edge (indegree~$>0$). Although it may happen temporarily, it
%% is not ideal for any node to be both borrower and lender, as is the case for
%% node~2 in Figure~\ref{fig:borrowing-graph}(a).
%% Figure~\ref{fig:borrowing-graph}(b) shows a redistribution of the memory, where all
%% nodes have ownership of the same total amounts of memory capacity, but whereas
%% subfigure (a) has 12,000 remote page references (5,000+3,000+4,000), subfigure
%% (b) has only 9,000 remote page references.  The borrowing graph therefore
%% should be kept ``approximately'' bipartite.
%% 
%% \textbf{Preference for small indegrees:} If a node $x$ has a high indegree,
%% that means that a large number of other nodes are (potentially) storing data at
%% $x$. If node $x$ fails, then it is likely that these nodes are hosting VMs
%% whose data has been lost. The borrowing graph should therefore have small
%% maximum and mean indegrees.
%% 
%% \textbf{Rebalancing the borrowing graph using max--flow}: Before explaining the
%% mechanism to rebalance the borrowing graph, it is worth understanding what
%% causes the borrowing graph can become ``unbalanced''.  Firstly, when a node
%% requires extra memory it is necessary to rapidly satisfy that demand using free
%% pages from a nearby node, and the best choice for lender may already also be a
%% borrower and/or already have a high indegree.  Secondly, an important design
%% criterion for AR-Tmem was to allow experimentation in the global memory
%% management policies.  A common solution to the problems identified above
%% simplifies the design of such policies.  The memory manager should not
%% unnecessarily worsen these problems, but it need not avoid them entirely.
%% 
%% \begin{figure}[t]
%%     \centering
%%     \includegraphics[width=\columnwidth]{maxflow.jpg}
%%     \caption{Balancing the borrowing graph using max--flow}
%%     \label{fig:maxflow-graph}
%% \end{figure}
%% 
%% To balance the graph, the MM--M performs the following:
%% 
%% \begin{enumerate}
%%     \item It creates a maximal directed acyclic subgraph from the borrowing graph,
%%         by selecting a source and performing a depth-first traversal of the
%%         outgoing edges. It does this repeatedly (once for each connected
%%         component). It then removes all edges directly from any source
%%         (pure borrower) to any sink (pure lender).
%% 
%%     \item It creates the graph illustrated in Figure~\ref{fig:maxflow-graph}(a) from
%%         the borrowing graph, by adding a source vertex connected to every
%%         source node.  It also adds a sink vertex connected to every sink node
%%         (with edge label the number of free pages at the sink node).
%% 
%%     \item It uses a max--flow algorithm to find a maximum flow through the
%%         graph, as illustrated in Figure~\ref{fig:maxflow-graph}(a).
%% 
%%     \item It reassigns memory, using a combination of \textsf{Mem-Limit} and
%%         \textsf{Grant-Return} messages. Specifically, for edge $x$ to $y$ with
%%         flow $f$, it issues \textsf{Grant-Return}($f$, $y$) to node $x$.  It
%%         also issues \textsf{Grant-Any}($f$, $src$) to the sinks as appropriate
%%         to complete the flow.  Since the sinks do not and will not own any
%%         remote memory, they will respond by transferring ownership of their
%%         own pages.
%% \end{enumerate}


% \begin{figure}[t]
% \centering
% \includegraphics[width=\columnwidth]{states.jpg}
% \caption{Node state transition diagram}
% \label{fig:sec3-states}
% \end{figure}


\subsubsection{Leaving the AR-Tmem system}
\label{sec:shutdown}

To cleanly shutdown a node that is part of AR-Tmem, the following procedure
must be followed.  For large systems this may take some time. Note that if a
node fails, then the procedure in Section~\ref{sec:resiliency} is followed instead.

\begin{enumerate}
    \item The node $R$ that wishes to shutdown notifies the MM--M using a
        \textsf{Leave-Req} message. 

    \item In response to the \textsf{Leave-Req} message, the MM--M moves the
        node to the \textsf{Leaving} state (see Figure~\ref{fig:sec3-states}). It
        also sends a \textsf{Force-Return}$(R)$ message to all nodes in the
        system. Each recipient returns all of the pages at $R$ that it owns
        and will reject any such pages received in future \textsf{Grant}
        messages. If the latest statistics indicate that there would be
        insufficient memory to satisfy these requests, the MM--M will issue
        \textsf{Grant-Any} messages to other nodes to provide sufficient
        memory, and it will also send \textsf{Mem--Limit} messages to ensure
        that the granted memory capacity is not allocated to the guests.

    \item Node $R$ frees all pages used by Tmem and returns ownership of all
        remote pages to their home nodes.  It does this as usual using
        peer-to-peer \textsf{Grant} messages.

    \item Periodically, each node sends a \textsf{Grant} message to node $R$ to
        return ownership of the pages that it had borrowed.

    \item Once the MM--M has received a \textsf{Statistics} message from every
        node indicating that $R$ is disabled and that it owns no pages at $R$,
        then the MM--M moves $R$ to the \textsf{Inactive} state
        (Figure~\ref{fig:sec3-states}) and sends a \textsf{Leave-Notify}
        message to $R$.  

    \item At this point the node $R$ has ownership of all its non-leaked pages,
        has left AR-Tmem, and may shutdown.

\end{enumerate}

\subsubsection{Resilient memory capacity aggregation}
\label{sec:resiliency}

\begin{figure}[!t]
\centering
\includegraphics[width=2.6in]{sec3-statesdiagram.pdf}
\caption{Node state transition diagram (at MM--M)}
\label{fig:sec3-states}
\end{figure}

There are two aspects to ensuring resiliency in the face of node failures:
protecting the integrity of the data stored in Tmem, and restoring lost system
state.

\label{sec:invalidation}

\textbf{Ensuring data integrity on slave failure}: The Tmem interface
guarantees that the data returned by a \textsf{get} operation must exactly
match the data previously written into Tmem by the corresponding \textsf{put}.
In addition, if the pool is \textit{permanent}, the \textsf{get} operation must
succeed.

\looseness -1 Assume that node $A$ has ownership of memory with home node $H$,
meaning that physical memory at node $H$ is used to store data written into
Tmem by VMs at $A$. If node $H$ fails, then this data is lost. If $H$ is then
rebooted, it will restart and may even attempt to re-join AR-Tmem. Clearly a
\textsf{get} operation performed at $A$ must never access the new contents of
the physical page at $H$, which would contain
incorrect data.
\begin{enumerate}

    \item When any node is booted, all remote memory accesses to it must raise
        a hardware exception (see Section~\ref{sec:sharing:arch}). If a Tmem
        client's \textsf{get} of a page causes an attempt to read a page at a
        failed node, this exception will be raised. If the page is
        \emph{permanent}, then the corresponding VM must be
        shutdown.\footnote{In future work the Tmem interface could be extended
            to allow the OS to terminate only the affected process, but in
        practice many processes would likely be affected.} If the page is
        \emph{ephemeral}, the \textsf{get} simply fails. On any failed access,
        read or write, the page is returned to the corresponding Buddy
        allocator, and it is disabled.

  \item When the Memory Manager at node $R$ is initialized without previously
      being cleanly shutdown, it first sends an \textsf{Invalidate} message to
      the Memory Manager. 

  \item The Memory Manager Master moves the node to the \text{Recovery} state
      (Figure~\ref{fig:sec3-states}) and sends an \textsf{Invalidate}($R$)
      message to all nodes.  On receipt, each node disables node $R$, and if it
      has ownership of pages with home $R$, it makes an \textsf{Invalidate-Xen}
      hypercall to invalidate the pages.

    \item Once the MM--M has received a \textsf{Statistics} message from every
        node, i.e.\ it has disabled $R$ and owns no pages at $R$, then the
        MM--M moves the node to the \textsf{Inactive} state and sends an
        \textsf{Invalidate-Notify} message to R.

    \item On receipt of the \textsf{Invalidate-Notify} message, node $R$ begins its
        normal initialization procedure.
\end{enumerate}

In the most general failure model, node $R$ could fail in a general way,
including arbitrary memory writes.  If such resiliency is required, it would be
necessary to protect all pages written into Tmem using a CRC.

\textbf{Restoring lost pages on slave failure}: Following a failure of a given
node $R$, the current design has no method to recover the pages that were owned
by node $R$. It is possible to approximate the number of lost pages from a
given home node using the statistics sent by the nodes, by adding up the number
of pages owned (across the nodes), and comparing against the total number of
physical pages at the node. However, since this is done using potentially
outdated information from \textsf{Statistics} messages, the value will not be
exact.  When this total is consistently below its desired value (equal to the
total physical memory at that node), the system administrator can be notified
to restart the node when convenient.

\textbf{Restoring system state on MM--M failure}: If the Memory Manager on the
Master fails, the system will continue to work, even nodes that are using
remote memory, except:

\begin{enumerate}
    \item No memory capacity will be redistributed among nodes
    \item No node can enter or leave the system.
\end{enumerate}

\looseness -1 When the MM--M attempts to restart, the MM--M will start
listening for connections from the other nodes.  When a node connects and
starts sending statistics using the \textsf{Statistics} message, that node is
added to the system in the \textsf{Active} state. A node that is currently in
the process of leaving or invalidation will send a new \textsf{Leave-Req} or
\textsf{Invalidation} message on re-connecting to the MM--M, which will restart
the leaving or invalidation process. At any time, all nodes in the
\textsf{Active} state are part of the normal global memory allocation.



\subsection{Hardware Support for Memory Aggregation}
\label{sec:sharing:arch}

In order to pool the memory resources across multiple nodes, AR-Tmem requires
the underlying hardware to provide the following features:

\begin{enumerate}

\item A fast interconnect, in order to provide a synchronous interface across
    the NUMA architecture inherent in distributed memory systems.

\item Direct memory access from the hypervisor to all the memory available: the
    hypervisor, when performing a tmem operation on behalf of a DomU, has to be
    able to access transparently its local and remote memory through the same
    mechanisms. This could be either through load/store instructions or through
    explicit RDMA support on the hypervisor.

\item Remote access to a node's pages is disabled on hardware boot, raising a
    hardware exception on the initiating node. Access is enabled by software,
    when the node joins AR-Tmem when it sends the \textsf{Register} message.

\item Given a physical address, it must be possible to extract the Node ID, for
    example by extracting a certain number of higher-order bits. 

\end{enumerate}

