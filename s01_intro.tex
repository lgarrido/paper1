\section{Introduction}

% New hardware
Current data centres are built using a large number of individual ``share
nothing'' servers, each provisioned with CPU, memory and I/O interfaces,
communicating over an Ethernet (or similar) network.  Recent work, however, has
proposed new system
architectures~\cite{durand2014euroserver,dongvenice,dredbox2016katrinis} that
leverage fast interconnects to share physical resources through the memory
hierarchy.  These architectures provide a shared global physical address space,
with memories addressable by every node at low latency using Remote DMA~(RDMA)
and ordinary load/store instructions.  These systems are composed of
\emph{coherence islands}, with cache coherency only enforced within an island
(\emph{node} in the rest of the paper). Across the system as a whole, however,
there is no hardware cache coherence, so the OSs or hypervisors must ensure
that each cache line is cached in at most one node.

\looseness -1 In current cloud environments, each node's physical memory
capacity is distributed by the hypervisor among one or more Virtual
Machines~(VMs), each hosting a guest OS.  The guest OSs have varying memory
demands, due to varying workloads and variable effectiveness of memory-saving
mechanisms such as same-page merging.  To improve utilization, physical memory
is often overcommitted, meaning that there is not enough memory for each guest
OS to be given all of the memory it was configured with at boot time. The
physical memory given to a VM is usually adjusted using memory ballooning
and/or memory hotplug.  Xen's Transcendent
Memory~(Tmem)~\cite{magenheimer2009transcendent} is way to make additional
memory capacity available to the VMs, through a paravirtualized put--get
interface.  By bringing underutilized memory capacity under the control of
Tmem, the hypervisor is able to quickly respond to increases in VM memory
demands.

% Introduce AR-Tmem
\looseness -1 This paper presents AR-Tmem (Aggregated Remote Tmem), an
extension of Tmem that allows the whole system's memory capacity to be shared
among all VMs in the system.  Most of the complexity of AR-Tmem is placed in a
user-space memory manager. AR-Tmem introduces minimal changes to the
hypervisor, as needed to enforce memory constraints, allocate local and remote
pages and pass ownership in and out of the hypervisor, and collect statistics
for the user-space memory manager.  Using this design, the hypervisor remains
small, secure and self-contained, while the user-space memory manager, in the
privileged domain, can support sophisticated memory management strategies and
inter-node communication. The user-space memory manager also enables nodes to
dynamically enter and leave the system and it handles fairness and resiliency.


The main contributions of this paper are:
\begin{enumerate}[topsep=2pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
\item A software architecture to share the system's aggregate memory capacity
    among all VMs in the system.
\item A two-tier user-space mechanism for allocation of aggregated (local and
    remote) memory capacity.
\item A performance evaluation using the CloudSuite~3.0 benchmarks.
\end{enumerate}

%1. A general approach to manage memory capacity in a distributed computing system implementing virtualization technoology.
%2. A modeling methodology for a computer system architecture with the characteristics of ZZZ, and a way to estimate its performance.

The rest of this paper is organized as follows. Section~\ref{sec:background}
gives the necessary background on virtualization, Tmem, and hardware coherence
islands.  Section~\ref{sec:sharing} presents AR-Tmem, including the mechanisms
in all software layers: hypervisor, kernel and user-space control. We also
explain how to provide resiliency to hardware failures and summarise the
necessary hardware support.  Section~\ref{sec:evaluation} describes the
experimental methodology and Section~\ref{sec:results} shows the evaluation
results.  Section~\ref{sec:relatedwork} compares with related work.  Finally,
Section~\ref{sec:conclusions} concludes the paper and outlines future work.
