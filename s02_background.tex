\section{Background}
\label{sec:background}

This section gives an overview of virtualization and memory 
management in existing cloud data centres. It then explains Xen 
transcendent memory.  Finally, it outlines the important 
characteristics of hardware coherence islands.

% Section 2.1 explains memory management in distributed memory systems from a
% very general perspective. Section 2.2 explains how virtualization technology
% can be deployed in distributed memory environments to manage memory. Section
% 2.3 explains further details of global address space and the architecture we
% use as our analytical framework.

\subsection{Virtualization In IaaS Clouds}
\label{sec:virtualization}

% Paraphrase NIST definition of cloud computing:
% http://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-145.pdf
% No need to cite, as overkill.

\looseness -1 Cloud computing provides on-demand access to an apparently
unlimited pool of compute, network and storage capacity.  There are multiple
cloud computing service models, but the most fundamental is
Infrastructure-as-a-Service~(IaaS), which allows the customer to rapidly deploy
Virtual Machines~(VMs) to execute arbitrary OSs and applications in the cloud.
IaaS shares the underlying hardware memory resources among customers using
virtualization software known as a hypervisor, e.g.\ Xen~\cite{barham2003xen},
KVM~\cite{kivity2007kvm} or VMWare~\cite{vmware}.


\subsubsection{Memory Management in the Hypervisor}
\label{sec:virtsinglenode}

% resource fragmentation
% if more memory resources are needed, then one or more VMs must be migrated to another node, incurring performance and energy overhead.

The hypervisor virtualizes the physical compute, I/O and memory resources of
the node on which it is running, independently of the resources in the other
nodes. The hypervisor creates and manages Virtual Machines (VMs), each of which
runs its own (guest) Operating System.  

When a VM is created, it is allocated a portion of the physical memory capacity
by the hypervisor. If a VM later requires more memory than this, a situation
called \emph{memory under-provisioning}, it will start swapping to its
(virtual) disk device, even if some of the node's physical memory is free (not
assigned to a VM) or has been assigned to a VM that does not currently need it.
The opposite case is \emph{memory overprovisioning}, where a VM has more memory
than needed; in this case the underutilized memory will be used for disk
caches, even though another VM may be swapping to its disk.  In the latter
case, it is beneficial to re-allocate memory among VMs.

% This is what is called memory under-provisioning: a virtual machine is
% actually using more memory than initially anticipated.

There already exist solutions for dynamically re-allocat\-ing idle memory among
VMs, including memory ballooning and memory hotplug, both of which are
implemented in the Xen hypervisor and Linux kernel.  Ballooning and memory
hotplugging are slow to respond, especially in reclaiming memory, requiring
prediction or leaving physical memory unused (``\emph{fallow}'') in the
hypervisor~\cite{magenheimer2009transcendent}.  However, these solutions do not
provide optimal memory resource utilization since important details regarding
the memory demand and usage by the VMs remain hidden from the hypervisor.

\begin{figure}[!t]
\centering
\includegraphics[width=2.8in]{sec2-tmembaseline}\vspace{-3mm}
% where an .eps filename suffix will be assumed under latex, 
% and a .pdf suffix will be assumed for pdflatex; or what has been declared
% via \DeclareGraphicsExtensions.
\caption{Using Xen Transcendent Memory (Tmem)}
\label{fig:sec2-tmembaseline}
\end{figure}

\subsubsection{State-of-the-Art Transcendent Memory}
\label{sec:tmem}

\looseness -1 Transcendent memory (Tmem)~\cite{magenheimer2009transcendent} was introduced as
a solution for dynamic memory management able to overcome some of the
limitations of memory ballooning and memory hotplug. Tmem works by pooling all
the node's physical pages that are under-utilized by VMs and/or that
have not been assigned to any VM. Tmem is abstracted as a key--value
store in which pages are written and read through a put--get interface.  The pools
can either be \emph{ephemeral} (e.g.\ clean data, which the hypervisor can
reclaim) or \emph{permanent} (e.g.\ dirty data, which the hypervisor has to
maintain) and either \emph{private} (to a VM) or \emph{shared} (among VMs). The
VMs remain agnostic to the total capacity of transcendent memory present and to
the physical locations of data stored in it.

In Linux, Tmem has two modes of operation: cleancache and frontswap, which have
been part of the kernel since Linux 3.0 (July 2011) and Linux 3.5 (July 2012),
respectively.  Figure~\ref{fig:sec2-tmembaseline} illustrates how cleancache
and frontswap are implemented within a VM. In order to enable the VMs to make
use of Tmem in either of the two modes, it is necessary for them to have a Tmem
kernel module (TKM) which handles all accesses to the Tmem pages on behalf of
the VM by issuing hypercalls to the hypervisor.

\begin{figure}[!t]
\centering
\hspace*{-2mm}\includegraphics[width=2.9in]{sec2-euroblkdiagram}
\vspace{-3mm}

\emph{(a) Hardware architecture}

\hspace*{-2mm}\includegraphics[width=2.9in]{sec2-genswstack}
\vspace{-3mm}

\emph{(b) Software architecture}
%\caption{Hardware and software architecture with coherence islands (two nodes)}
\caption{Coherence islands (with two nodes)}
\label{fig:sec2-euroblkdiagram}
\end{figure}

% inspired by linux-3.19/Documentation/vm/cleancache.txt
Linux cleancache is a victim cache for clean pages that are evicted by the
Linux kernel's Pageframe Replacement Algorithm (PFRA). Since clean data can be
re-read from disk (the normal behavior without cleancache), the page is copied into Tmem as
ephemeral data.  When a filesystem wishes to access a page from disk, it first
checks the cleancache. If the page is found, then the data is requested via the
Tmem interface. If still available, the data is copied direct to the kernel's
VM, avoiding the usual access to disk.

% inspired by linux-3.19/Documentation/vm/frontswap.txt
Linux frontswap uses Tmem as a cache in front of a swap device.  When a page is
stored to the swap device, frontswap first attempts to \textsf{put} the page
into Tmem as permanent data. If the \textsf{put} fails, then the page is
written to swap space, as normal. Whenever the VM re-loads a page from swap,
frontswap will first check whether it was previously \textsf{put} into Tmem. If
so, it will \textsf{get} it from Tmem, which will succeed (because the page is
permanent).  A successful store (\textsf{put}) to Tmem will therefore avoid the
disk write and read. 

The Tmem interface supports two flush operations: \textsf{flush-page} and
\textsf{flush-object}. Flush operations are generated when the page contents
are no longer required, for example because they belong to a process that has
finished executing or because a VM wishes to invalidate it. The flush
operations assist the hypervisor to efficiently use Tmem
capacity~\cite{magenheimer2009transcendent}. When a flush is issued, the page
or pages are freed by the domain and can be used to \textsf{put} additional VM
page(s).

%To pool the resources together of all the nodes in a virtualized way, cloud
%computing solutions rely on a special software that executes as a middleware across the nodes of the computing infrastructure. 

%In this approach, all the memory management is done by a cloud software that orders the hypervisor to create VMs and tells the hypervisor the resources to assign to each VM. Then, the processes are executed inside this VMs. When a VM requires more memory, it will rely first try to reuse its own local memory. If it is not possible, the VM will have to be migrated (ordered by the cloud software) to a computer that will have enough memory to meet the demand. 

% Our approach is fundamentally different. We don't intend to implement a middleware that controls the allocation of memory to processes or that monitors the VMs created in the system. We intend to use a distributed mechanism of service and request, and to extend the resources available to a VM across multiple nodes of the computing infrastructure. We will explain the details of our approach in Section 3.  

%\subsection{Memory Management in Distributed Memory Systems}
%\begin{figure}[!t]
%\centering
%\includegraphics[width=3.4in]{figure1.jpg}
% where an .eps filename suffix will be assumed under latex, 
% and a .pdf suffix will be assumed for pdflatex; or what has been declared
% via \DeclareGraphicsExtensions.
%\caption{Diagram of a core cluster based on NVIDIA's Kepler GeForce GTX 680 %GPGPU. (a) The core cluster with its internal hardware modules. (b) %Illustration of the thread hierarchy in the SIMT programming model.}
%\label{clusterDiagram}
%\end{figure}

\subsection{Hardware Support for Coherence Islands}
\label{sec:archmodel}

Recent advances in computer architecture research have proposed 
architectures with a shared global physical address space but
no global hardware cache coherence.  Examples include Venice~\cite{dongvenice}
and EUROSER\-VER~\cite{durand2014euroserver}, both based on the ARM architecture,
as well as the Intel Single-Chip Cloud computer~\cite{howard201048}, dRedBox~\cite{dredbox2016katrinis}
and Beehive computer~\cite{thacker2010beehive}.

The essential features of these systems are represented in
Figure~\ref{fig:sec2-euroblkdiagram}(a).  Each node contains $N$ processor
cores, connected in clusters via a local cache-coherent interconnect to local
DRAM and I/O devices.  Remote memory is visible through the global physical
address space, and communication across the nodes is achieved through an
\emph{inter-node interface} and \emph{global interconnect} responsible for
routing remote memory accesses to the appropriate node. 

Depending on the hardware architecture and page table settings, remote accesses
can be uncached, cached at the \emph{home} node (physically attached to the
memory) or cached at the user (where the corresponding load or store is
initiated).  Although caching of remote accesses is useful in general, the Tmem
approach naturally has poor temporal locality, so it is not necessary to cache
remote accesses.

The essential characteristics of this architecture are:
\begin{itemize}
\item Each node executes its own hypervisor and OSs.
\item A global physical memory address space provides low-latency access to memory in all nodes.
\item Routing is based on the global physical address (e.g. high-order bits).
\item Fast communication is provided across the nodes of the system, bypassing network protocols~(e.g. TCP/IP).
%\item Features required for resiliency, as explained in Section~\ref{sec:resiliency}.
\end{itemize}

%% === Not background: should be in intro or the AR-Tmem design:
%% In this work, we use Tmem as the means to aggregate memory resources among
%% nodes. When a VM uses more memory than assigned to it, and the node is
%% unable to use its own memory, then it will be necessary to get additional
%% memory from another node. As long as the access latency to a remote node is
%% less than an access to the disk/swap device, then this approach will most
%% likely yield an important performance improvement. 

%% === Briefly above: moved to AR-Tmem hardware requirements
%% Given a physical address, it must be possible to extract the Node ID, for
%% example by extracting a certain number of higher-order bits. 

%% == This is related work
%% This way of exploiting Tmem has also been used by mechanisms such as RAMster, which 
%% is a peer-to-peer implementation of Tmem~\cite{ramster}. In the proof-of-concept 
%% implementation, communication is done through an standard ethernet connection. In our work,
%% we rely on the global physical memory address space to reach the nodes, and use
%% additional software components for efficient memory aggregation and management, 
%% as we shall see in the next section.

