all:sigplanconf-template.pdf

sigplanconf-template.pdf: sigplanconf-template.tex \
    s00_abstract.tex \
    s01_intro.tex \
    s02_background.tex \
    s03_mcs_tmem.tex \
    s04_methodology.tex \
    s05_results.tex \
    s06_related_work.tex \
    s07_conclusions.tex \
    ref.bib
	pdflatex sigplanconf-template
	bibtex sigplanconf-template
	pdflatex sigplanconf-template

clean:
	rm -f sigplanconf-template.{pdf,log,aux,bbl}
